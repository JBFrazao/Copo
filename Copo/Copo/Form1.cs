﻿using System;
using System.Windows.Forms;

namespace Copo
{
    public partial class Bar : Form
    {
        Timer MyTimer = new Timer();
        Timer Medidor = new Timer();
        int i = 0;
        int j = 0;
        int y = 410;
        int x = 341;
        string liquido = "Água";
        int capacidade = 120, contem = 0;
        bool copoCriado = false;
        private Copo novoCopo;

        public Bar()
        {
            InitializeComponent();
            MyTimer.Interval = (5);
            MyTimer.Tick += new EventHandler(MyTimer_Tick);
            Medidor.Interval = (5);
            Medidor.Tick += new EventHandler(Medidor_Tick);
            Medidor.Start();

        }

        private void MyTimer_Tick(object sender, EventArgs e)
        {
            if (liquid.Height < contem)
            {
                i--;
                liquid.Height++;
                liquid.Location = new System.Drawing.Point(x+5, y+i);
            }
            else if (liquid.Height > contem)
            {
                i++;

                liquid.Height--;
                liquid.Location = new System.Drawing.Point(x+5, y + i);
            }
            else if (liquid.Height == contem)
            {
                MyTimer.Stop();
                ContemScroll.Enabled = true;
            }

        }   //temporizador do enchimento

        private void Medidor_Tick(object sender, EventArgs e)
        {

            if (Medicao.Height < contem)
            {
                j--;
                Medicao.Height++;
                Medicao.Location = new System.Drawing.Point(x + 5, y + j);
            }
            else if (Medicao.Height > contem)
            {
                j++;
                Medicao.Height--;
                Medicao.Location = new System.Drawing.Point(x + 5, y + j);
            }

            if (liquid.Height <= Medicao.Height)
            {
                liquid.BringToFront();
            }
            else
            {
                Medicao.BringToFront();
            }
           
        }   //temporizador do medidor

        private void NovoCopo_Click(object sender, EventArgs e)
        {
            copoCriado = true;
            
            MyTimer.Start();

            ContemScroll.Enabled = false;

            novoCopo = new Copo(liquido, capacidade);
            novoCopo.Encher(contem);

            Percentage.Text = novoCopo.ValorEmPercentagem().ToString() + " %";

            MensagemString.Text = novoCopo.ToString();

            
        }   //criar um copo novo

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CapacidadeComboBox.Text.StartsWith("1"))
            {
                capacidade = 120;
                Copo.Height = capacidade+20;
                Copo.Location = new System.Drawing.Point(x, y - capacidade);
                
                if (liquid.Height > capacidade)
                {
                    
                    liquid.Height = capacidade;
                    i = 0-liquid.Height;
                    liquid.Location = new System.Drawing.Point(x+5, y + i);

                }
                if (Medicao.Height > capacidade)
                {                    
                    Medicao.Height = capacidade;
                    j = 0-Medicao.Height;
                    Medicao.Location = new System.Drawing.Point(x + 5, y + j);
                }

            }
            else if (CapacidadeComboBox.Text.StartsWith("2"))
            {
                capacidade = 250;
                Copo.Height = capacidade+20;
                Copo.Location = new System.Drawing.Point(x, y - capacidade);
                if (liquid.Height > capacidade)
                {

                    liquid.Height = capacidade;
                    i = 0 - liquid.Height;
                    liquid.Location = new System.Drawing.Point(x + 5, y + i);

                }
                if (Medicao.Height > capacidade)
                {
                    Medicao.Height = capacidade;
                    j = 0 - Medicao.Height;
                    Medicao.Location = new System.Drawing.Point(x + 5, y + j);
                }
            }
            else if (CapacidadeComboBox.Text.StartsWith("4"))
            {
                capacidade = 400;
                Copo.Height = capacidade+20;
                Copo.Location = new System.Drawing.Point(x, y - capacidade);
               


            }

            ContemScroll.Maximum = capacidade/10;

            

            if (contem > capacidade)
            {
                Percentagem.Text = ContemScroll.Maximum*10 + "/" + CapacidadeComboBox.Text;
                ContemScroll.Value = capacidade / 10;
                contem = ContemScroll.Value * 10;
                
            }
            else
            {
                Percentagem.Text = ContemScroll.Value * 10 + "/" + CapacidadeComboBox.Text;
                contem = ContemScroll.Value * 10;
            }

            if (copoCriado == true)
            {
                novoCopo.Capacidade = capacidade;
                novoCopo.Contem = contem;
                Percentage.Text = novoCopo.ValorEmPercentagem().ToString() + " %";
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            contem = ContemScroll.Value *10;
            Percentagem.Text = Convert.ToString(contem) + "/" + CapacidadeComboBox.Text;
        }

        private void Bar_Load(object sender, EventArgs e)
        {

        }

        private void ComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            liquido = LiquidoComboBox.Text;
            if (LiquidoComboBox.Text.StartsWith("Ág"))
            {
                liquid.BackColor = System.Drawing.Color.LightBlue;
               

            }
            else if (LiquidoComboBox.Text.StartsWith("Co"))
            {
                liquid.BackColor = System.Drawing.Color.Brown;

            }
            else if (LiquidoComboBox.Text.StartsWith("Ce"))
            {
                liquid.BackColor = System.Drawing.Color.Yellow;

            }
        }
    }
}
