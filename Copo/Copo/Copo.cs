﻿using System;
using System.Dynamic;

namespace Copo
{
    /// <summary>
    /// Classe que define uma representação para copos de líquidos,
    /// que podem ser enchidos, esvaziados, etc.
    /// </summary>
    public class Copo
    {
        #region Atributos

        //private string _liquido;

        //private double _capacidade;    NAO UTILIZEI...

        //private double _contem;

        #endregion


        #region Propriedades

        public string Liquido { get; set; }

        public double Capacidade { get; set; }

        public double Contem { get; set; }

        #endregion

        #region Construtores

        //public Copo()
        //{
        //    Liquido = "??";
        //    Capacidade = 25.0;
        //    Contem = 0.0;
        //}

        public Copo() : this("??", 25.0) { }

        public Copo(string liquido, double quantidade)
        {
            Liquido = liquido;
            Capacidade = quantidade;
            Contem = 0.0;
        }

        public Copo(Copo c) : this(c.Liquido, c.Capacidade)
        {
            Contem = c.Contem;
        }

        #endregion


        #region Métodos Gerais

        public override string ToString()
        {
            return String.Format("O Copo com capacidade para {0} ml, tem {1} ml de {2}. ", Capacidade, Contem, Liquido);
        }

        #endregion

        #region Métodos

        public void Encher(double valor)
        {
            if (Contem + valor >= Capacidade)
            {
                Contem = Capacidade;
            }
            else
            {
                Contem += valor;
            }
        }

        public void Esvaziar(double valor)
        {
            double aux = Contem - valor;
            Contem = (aux <= 0.0) ? 0.0 : aux;
        }

        public bool Cheio()
        {
            return Contem.Equals(Capacidade);
        }

        public int ValorEmPercentagem()
        {
            return (int)Math.Round(Contem / Capacidade * 100);
            
        }

        #endregion
    }
}

