﻿namespace Copo
{
    partial class Bar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NovoCopo = new System.Windows.Forms.Button();
            this.ContemScroll = new System.Windows.Forms.TrackBar();
            this.Percentagem = new System.Windows.Forms.Label();
            this.LiquidoComboBox = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.CapacidadeComboBox = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.liquid = new System.Windows.Forms.PictureBox();
            this.Copo = new System.Windows.Forms.PictureBox();
            this.Medicao = new System.Windows.Forms.PictureBox();
            this.MensagemString = new System.Windows.Forms.Label();
            this.Percentage = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ContemScroll)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.liquid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Copo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Medicao)).BeginInit();
            this.SuspendLayout();
            // 
            // NovoCopo
            // 
            this.NovoCopo.BackColor = System.Drawing.Color.White;
            this.NovoCopo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.NovoCopo.ForeColor = System.Drawing.Color.Indigo;
            this.NovoCopo.Location = new System.Drawing.Point(12, 561);
            this.NovoCopo.Name = "NovoCopo";
            this.NovoCopo.Size = new System.Drawing.Size(786, 38);
            this.NovoCopo.TabIndex = 0;
            this.NovoCopo.Text = "ENCHER / ESVAZIAR";
            this.NovoCopo.UseVisualStyleBackColor = false;
            this.NovoCopo.Click += new System.EventHandler(this.NovoCopo_Click);
            // 
            // ContemScroll
            // 
            this.ContemScroll.Location = new System.Drawing.Point(12, 508);
            this.ContemScroll.Maximum = 12;
            this.ContemScroll.Name = "ContemScroll";
            this.ContemScroll.Size = new System.Drawing.Size(692, 45);
            this.ContemScroll.TabIndex = 1;
            this.ContemScroll.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.ContemScroll.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // Percentagem
            // 
            this.Percentagem.AutoSize = true;
            this.Percentagem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Percentagem.ForeColor = System.Drawing.Color.Yellow;
            this.Percentagem.Location = new System.Drawing.Point(707, 520);
            this.Percentagem.Name = "Percentagem";
            this.Percentagem.Size = new System.Drawing.Size(69, 20);
            this.Percentagem.TabIndex = 4;
            this.Percentagem.Text = "0/120 ml";
            // 
            // LiquidoComboBox
            // 
            this.LiquidoComboBox.ForeColor = System.Drawing.Color.BlueViolet;
            this.LiquidoComboBox.FormattingEnabled = true;
            this.LiquidoComboBox.ItemHeight = 13;
            this.LiquidoComboBox.Items.AddRange(new object[] {
            "Água",
            "Coca-Cola",
            "Cerveja"});
            this.LiquidoComboBox.Location = new System.Drawing.Point(6, 19);
            this.LiquidoComboBox.Name = "LiquidoComboBox";
            this.LiquidoComboBox.Size = new System.Drawing.Size(374, 21);
            this.LiquidoComboBox.TabIndex = 3;
            this.LiquidoComboBox.Text = "Água";
            this.LiquidoComboBox.SelectedIndexChanged += new System.EventHandler(this.ComboBox2_SelectedIndexChanged);
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.ItemHeight = 13;
            this.comboBox3.Items.AddRange(new object[] {
            "Água",
            "Coca-Cola",
            "Cerveja"});
            this.comboBox3.Location = new System.Drawing.Point(6, 67);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(353, 21);
            this.comboBox3.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBox3);
            this.groupBox1.Controls.Add(this.LiquidoComboBox);
            this.groupBox1.ForeColor = System.Drawing.Color.Yellow;
            this.groupBox1.Location = new System.Drawing.Point(12, 452);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(386, 50);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bebida";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.CapacidadeComboBox);
            this.groupBox2.ForeColor = System.Drawing.Color.Yellow;
            this.groupBox2.Location = new System.Drawing.Point(423, 452);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(375, 50);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Capacidade do Copo";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.ItemHeight = 13;
            this.comboBox1.Items.AddRange(new object[] {
            "Água",
            "Coca-Cola",
            "Cerveja"});
            this.comboBox1.Location = new System.Drawing.Point(6, 67);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(353, 21);
            this.comboBox1.TabIndex = 4;
            // 
            // CapacidadeComboBox
            // 
            this.CapacidadeComboBox.ForeColor = System.Drawing.Color.BlueViolet;
            this.CapacidadeComboBox.FormattingEnabled = true;
            this.CapacidadeComboBox.ItemHeight = 13;
            this.CapacidadeComboBox.Items.AddRange(new object[] {
            "120 ml",
            "250 ml",
            "400 ml"});
            this.CapacidadeComboBox.Location = new System.Drawing.Point(6, 19);
            this.CapacidadeComboBox.Name = "CapacidadeComboBox";
            this.CapacidadeComboBox.Size = new System.Drawing.Size(363, 21);
            this.CapacidadeComboBox.TabIndex = 3;
            this.CapacidadeComboBox.Text = "120 ml";
            this.CapacidadeComboBox.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.SteelBlue;
            this.pictureBox1.Location = new System.Drawing.Point(-25, -26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(858, 472);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // liquid
            // 
            this.liquid.BackColor = System.Drawing.Color.LightBlue;
            this.liquid.Location = new System.Drawing.Point(346, 400);
            this.liquid.Name = "liquid";
            this.liquid.Size = new System.Drawing.Size(130, 0);
            this.liquid.TabIndex = 10;
            this.liquid.TabStop = false;
            // 
            // Copo
            // 
            this.Copo.BackColor = System.Drawing.Color.LightCyan;
            this.Copo.Location = new System.Drawing.Point(341, 290);
            this.Copo.Name = "Copo";
            this.Copo.Size = new System.Drawing.Size(140, 140);
            this.Copo.TabIndex = 11;
            this.Copo.TabStop = false;
            // 
            // Medicao
            // 
            this.Medicao.BackColor = System.Drawing.Color.Tan;
            this.Medicao.Location = new System.Drawing.Point(346, 400);
            this.Medicao.Name = "Medicao";
            this.Medicao.Size = new System.Drawing.Size(130, 0);
            this.Medicao.TabIndex = 12;
            this.Medicao.TabStop = false;
            // 
            // MensagemString
            // 
            this.MensagemString.AutoSize = true;
            this.MensagemString.BackColor = System.Drawing.Color.SteelBlue;
            this.MensagemString.Location = new System.Drawing.Point(500, 430);
            this.MensagemString.Name = "MensagemString";
            this.MensagemString.Size = new System.Drawing.Size(0, 13);
            this.MensagemString.TabIndex = 13;
            // 
            // Percentage
            // 
            this.Percentage.AutoSize = true;
            this.Percentage.BackColor = System.Drawing.Color.SteelBlue;
            this.Percentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Percentage.Location = new System.Drawing.Point(550, 309);
            this.Percentage.Name = "Percentage";
            this.Percentage.Size = new System.Drawing.Size(154, 91);
            this.Percentage.TabIndex = 14;
            this.Percentage.Text = "0%";
            // 
            // Bar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Goldenrod;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(810, 611);
            this.Controls.Add(this.Percentage);
            this.Controls.Add(this.liquid);
            this.Controls.Add(this.MensagemString);
            this.Controls.Add(this.Medicao);
            this.Controls.Add(this.NovoCopo);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Percentagem);
            this.Controls.Add(this.ContemScroll);
            this.Controls.Add(this.Copo);
            this.Controls.Add(this.pictureBox1);
            this.ForeColor = System.Drawing.SystemColors.Window;
            this.Name = "Bar";
            this.Text = "Bar";
            this.Load += new System.EventHandler(this.Bar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ContemScroll)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.liquid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Copo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Medicao)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button NovoCopo;
        private System.Windows.Forms.TrackBar ContemScroll;
        private System.Windows.Forms.Label Percentagem;
        private System.Windows.Forms.ComboBox LiquidoComboBox;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox CapacidadeComboBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox liquid;
        private System.Windows.Forms.PictureBox Copo;
        private System.Windows.Forms.PictureBox Medicao;
        private System.Windows.Forms.Label MensagemString;
        private System.Windows.Forms.Label Percentage;
    }
}

